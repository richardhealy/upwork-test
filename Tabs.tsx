import React from 'react';
import _ from 'lodash';
import shallowCompare from 'react-addons-shallow-compare';
import { setPropValue } from 'react-updaters';

type TabsProp = {
  label: string;
  value: any;
  liClassName?: string;
  aClassName?: string;
};

type OnChangeFn = () => any; // Need more info to define this

interface Props {
  tabs: TabsProp | TabsProp[];
  onChange: OnChangeFn;
  activeTab?: string | number;
}

/** A set of Bootstrap tabs that you can toggle between for navigation */
export class Tabs extends React.Component<Props> {
  public shouldComponentUpdate(nextProps: Props, nextState: {}): boolean {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { activeTab } = this.props;
    let { tabs } = this.props;

    if (!(tabs instanceof Array)) {
      tabs = _.map(tabs, (label, value) => ({ label, value }));
    }

    return (
      <ul className="nav nav-tabs" role="navigation">
        {tabs.map(({ label, value, liClassName, aClassName }) => (
          <li
            key={value}
            className={
              (activeTab === value ? 'active ' : '') + (liClassName || '')
            }
          >
            <a
              href={`#openTab${value}`}
              className={aClassName}
              onClick={setPropValue(this, 'onChange', 'tab', null, value, true)}
            >
              {label}
            </a>
          </li>
        ))}
      </ul>
    );
  }
}
export default Tabs;
